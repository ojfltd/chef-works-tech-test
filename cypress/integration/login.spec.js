import en from '../../src/translations/en.json';
import fr from '../../src/translations/fr.json';

describe('When a user lands on the login screen', () => {
  let fixture;

  before(() => {
    cy.visit('/');

    cy.fixture('login').then(login => {
      fixture = login;
    });
  });

  it('a username input is visible', () => {
    cy.get('[data-testid="username-input"]').should('be.visible');
  });

  it('a password input is visible', () => {
    cy.get('[data-testid="password-input"]').should('be.visible');
  });

  it('a submit button is visible', () => {
    cy.get('button[type="submit"]').should('be.visible');
  });

  xdescribe('entering invalid username and password', () => {
    before(() => cy.visit('/'));

    it('should display an error', () => {
      cy.get('[data-testid="username-input"]').type(fixture.badUsername);
      cy.get('[data-testid="password-input"]').type(fixture.badPassword);

      cy.get('button[type="submit"]').click();

      cy.get('[data-testid="login-error"]').should('be.visible');
    });
  });

  describe('entering valid username and password', () => {
    before(() => cy.visit('/'));

    it('should display an dashboard', () => {
      cy.get('[data-testid="username-input"]').type(fixture.goodUsername);
      cy.get('[data-testid="password-input"]').type(fixture.goodPassword);

      cy.get('button[type="submit"]').click();

      cy.get('[data-testid="product-list"]').should('be.visible');
    });
  });

  describe('and wants to change the language', () => {
    before(() => cy.visit('/'));

    it('language links are visible', () => {
      ['en', 'fr'].forEach(locale =>
        cy.get(`[data-testid="locale-toggle-${locale}"]`).should('be.visible')
      );
    });

    it('clicking the french language link will update the labels', () => {
      cy.get('[for="username"]').contains(
        en['app.containers.Login.label.username']
      );
      cy.get('[for="password"]').contains(
        en['app.containers.Login.label.password']
      );
      cy.get('button[type="submit"]').contains(
        en['app.containers.Login.label.login']
      );

      cy.get('[data-testid="locale-toggle-fr"]').click();

      cy.get('[for="username"]').contains(
        fr['app.containers.Login.label.username']
      );
      cy.get('[for="password"]').contains(
        fr['app.containers.Login.label.password']
      );
      cy.get('button[type="submit"]').contains(
        fr['app.containers.Login.label.login']
      );
    });
  });
});
