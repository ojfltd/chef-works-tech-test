describe('When a user lands on the product list screen', () => {
  let fixture;

  before(() => {
    cy.visit('/');

    cy.fixture('login').then(login => {
      fixture = login;
    });
  });

  describe('and products are displayed', () => {
    before(() => {
      cy.get('[data-testid="username-input"]').type(fixture.badUsername);
      cy.get('[data-testid="password-input"]').type(fixture.badPassword);

      cy.get('button[type="submit"]').click();
    });

    it('a product count should be visible', () => {
      cy.get('[data-testid="product-count"]').should('be.visible');
      cy.get('[data-testid="product-count"]').contains('Showing 20 products');
    });

    it('a product count should be visible', () => {
      cy.get('[data-testid="product-count"]').should('be.visible');
    });

    it('size filters should be visible', () => {
      cy.get('[data-testid="filter-size"]').should('be.visible');
    });

    it('location filters should be visible', () => {
      cy.get('[data-testid="filter-location"]').should('be.visible');
    });

    it('type filters should be visible', () => {
      cy.get('[data-testid="filter-product_type"]').should('be.visible');
    });

    it('clicking a filter will change the product list', () => {
      cy.contains('TORQUE').click();
      cy.get('[data-testid="product-count"]').contains('Showing 12 products');
    });

    it('clicking a filter again change will remove it and update the product list', () => {
      cy.contains('TORQUE').click();
      cy.get('[data-testid="product-count"]').contains('Showing 20 products');
    });
  });
});
