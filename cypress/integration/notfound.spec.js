describe('When a user visits an invalid path', () => {
  before(() => cy.visit('/this-page-does-not-exist'));

  it('a title is displayed', () => {
    cy.get('span').contains('Page not found');
  });
});
