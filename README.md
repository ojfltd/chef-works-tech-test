# Chef Works Tech Test

Live demo: [https://festive-noyce-159512.netlify.com/](https://festive-noyce-159512.netlify.com/)

## Tech Overview

I've used the following tools and frameworks within this tech test:

- Redux for state management
- Sagas for handling side effects
- Tailwind CSS for style and layout
- Jest for unit testing
- Cypress for automated testing
- Storybook for building a component library

## `npm start`

Start the application in development mode.

## `npm test`

Run the unit tests

## `npm run cypress`

Run the automation tests
