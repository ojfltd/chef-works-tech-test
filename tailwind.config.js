module.exports = {
  theme: {
    extend: {
      width: {
        '96': '24rem'
      },
      maxWidth: {
        '3/4': '75%'
      }
    }
  },
  variants: {},
  plugins: []
};
