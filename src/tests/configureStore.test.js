import configureStore from '../configureStore';

describe('configureStore', () => {
  let store;

  beforeAll(() => {
    store = configureStore({});
  });

  // describe('injectedReducers', () => {
  //   test('should contain an object for reducers', () => {
  //     expect(typeof store.injectedReducers).toBe('object');
  //   });
  // });

  // describe('injectedSagas', () => {
  //   test('should contain an object for sagas', () => {
  //     expect(typeof store.injectedSagas).toBe('object');
  //   });
  // });

  describe('runSaga', () => {
    test('should contain a hook for `sagaMiddleware.run`', () => {
      expect(typeof store.runSaga).toBe('function');
    });
  });
});
