import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Login';

export default defineMessages({
  error: {
    id: `${scope}.error.username_or_password_incorrect`,
    defaultMessage: 'Username or password incorrect'
  },
  username: {
    id: `${scope}.label.username`,
    defaultMessage: 'Username'
  },
  password: {
    id: `${scope}.label.password`,
    defaultMessage: 'Password'
  },
  login: {
    id: `${scope}.label.login`,
    defaultMessage: 'Login'
  }
});
