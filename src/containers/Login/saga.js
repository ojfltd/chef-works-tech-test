import { push } from 'connected-react-router';
import { call, put, takeLatest } from 'redux-saga/effects';

import request from 'utils/request';
import { setAuthToken } from 'utils';

import { authenticateUserSuccess, authenticateUserError } from './actions';
import { AUTHENTICATE_USER_REQUESTED } from './constants';

export function* authenticate({ username, password }) {
  try {
    const { authKey: token } = yield call(
      request,
      'https://xznrqupkl2.execute-api.eu-west-1.amazonaws.com/test/login',
      {
        body: JSON.stringify({ username, password }),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: undefined
        }
      }
    );
    yield call(setAuthToken, token);
    yield put(authenticateUserSuccess());
    yield put(push('/dashboard'));
  } catch (error) {
    yield put(authenticateUserError(error));
  }
}

export default function* userData() {
  // Watches for AUTHENTICATE_USER_REQUESTED actions and calls authenticate when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(AUTHENTICATE_USER_REQUESTED, authenticate);
}
