import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import classNames from 'classnames';

import ErrorSummary from 'components/ErrorSummary';
import Button from 'components/Button';
import Field from 'components/Field';

import { locales } from 'utils/constants';
import { AUTHENTICATE_USER_REQUESTED } from './constants';

import { changeLocale } from '../LanguageProvider/actions';
import { authenticateUser } from './actions';

import messages from './messages';

export function Login({ onLocaleToggle, onFormSubmit, status, hasError }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = evt => {
    evt.preventDefault();
    return onFormSubmit({ username, password });
  };

  return (
    <>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <div className="flex flex-col items-center justify-center h-screen bg-gray-800">
        <form
          className="mb-5 bg-white rounded p-6 shadow-lg w-96 max-w-3/4"
          onSubmit={onSubmit}
        >
          {hasError ? (
            <ErrorSummary
              data-testid="login-error"
              title={<FormattedMessage {...messages.error} />}
            />
          ) : null}
          <Field
            required
            autoComplete="username"
            data-testid="username-input"
            id="username"
            label={<FormattedMessage {...messages.username} />}
            name="username"
            type="text"
            onChange={evt => setUsername(evt.target.value)}
          />
          <Field
            required
            autoComplete="current-password"
            data-testid="password-input"
            id="password"
            label={<FormattedMessage {...messages.password} />}
            name="password"
            type="password"
            onChange={evt => setPassword(evt.target.value)}
          />
          <Button
            full
            data-testid="login-button"
            disabled={status === AUTHENTICATE_USER_REQUESTED}
            type="submit"
          >
            <FormattedMessage {...messages.login} />
          </Button>
        </form>
        <div className="flex">
          {locales.map(locale => {
            const classes = classNames(
              'text-sm text-gray-500 hover:underline focus:underline focus:outline-none',
              {
                'mr-4': locale !== locales[locales.length - 1]
              }
            );

            return (
              <button
                key={locale.shortName}
                className={classes}
                data-testid={`locale-toggle-${locale.shortName}`}
                type="button"
                value={locale.shortName}
                onClick={onLocaleToggle}
              >
                {locale.displayName}
              </button>
            );
          })}
        </div>
      </div>
    </>
  );
}

Login.propTypes = {
  onLocaleToggle: PropTypes.func,
  onFormSubmit: PropTypes.func,
  status: PropTypes.string,
  hasError: PropTypes.bool
};

export const mapDispatchToProps = dispatch => ({
  onLocaleToggle: evt => dispatch(changeLocale(evt.target.value)),
  onFormSubmit: ({ username, password }) =>
    dispatch(authenticateUser({ username, password })),
  dispatch
});

export const mapStateToProps = ({
  language: { locale },
  auth: { status, error: hasError }
}) => ({
  locale,
  status,
  hasError
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
