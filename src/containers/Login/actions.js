import {
  AUTHENTICATE_USER_REQUESTED,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR
} from './constants';

export function authenticateUser({ username, password }) {
  return {
    type: AUTHENTICATE_USER_REQUESTED,
    username,
    password
  };
}

export function authenticateUserSuccess() {
  return {
    type: AUTHENTICATE_USER_SUCCESS,
    error: false
  };
}

export function authenticateUserError(error) {
  return {
    type: AUTHENTICATE_USER_ERROR,
    error
  };
}
