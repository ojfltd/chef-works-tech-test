import { push } from 'connected-react-router';
import { call, put, takeLatest } from 'redux-saga/effects';

import { AUTHENTICATE_USER_REQUESTED } from '../constants';
import { authenticateUserSuccess, authenticateUserError } from '../actions';
import userData, { authenticate } from '../saga';
import { setAuthToken } from '../../../utils';

const credentials = {
  username: 'username',
  password: 'password'
};

const response = {
  token: 'abc123'
};

describe('authenticate saga', () => {
  let generator;

  beforeEach(() => {
    generator = authenticate(credentials);
    generator.next();
  });

  test('should dispatch the authenticateUserSuccess action when successfull and redirect', () => {
    expect(generator.next({ authKey: 'abc' }).value).toEqual(
      call(setAuthToken, 'abc')
    );
    expect(generator.next(response).value).toEqual(
      put(authenticateUserSuccess())
    );
    expect(generator.next().value).toEqual(put(push('/dashboard')));
  });

  test('should dispatch the authenticateUserError action when unsuccessful', () => {
    const error = new Error('Some error');
    expect(generator.throw(error).value).toEqual(
      put(authenticateUserError(error))
    );
  });
});

describe('userData Saga', () => {
  const userDataSaga = userData();

  test('should start task to watch for AUTHENTICATE_USER_REQUESTED action', () => {
    const takeLatestDescriptor = userDataSaga.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(AUTHENTICATE_USER_REQUESTED, authenticate)
    );
  });
});
