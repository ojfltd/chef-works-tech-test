import loginReducer from '../reducer';
import {
  AUTHENTICATE_USER_REQUESTED,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR
} from '../constants';

describe('loginReducer', () => {
  test('returns the initial state', () => {
    expect(loginReducer(undefined, {})).toEqual({
      error: false,
      status: undefined
    });
  });

  test('updated the status when authentication is requested', () => {
    expect(
      loginReducer(undefined, {
        type: AUTHENTICATE_USER_REQUESTED
      })
    ).toEqual({
      status: AUTHENTICATE_USER_REQUESTED,
      error: false
    });
  });

  test('updates the status when authentication is successful', () => {
    expect(
      loginReducer(undefined, {
        type: AUTHENTICATE_USER_SUCCESS,
        token: 'abc123'
      })
    ).toEqual({
      status: AUTHENTICATE_USER_SUCCESS,
      error: false,
      token: 'abc123'
    });
  });

  test('updates the status and error when authentication is unsuccessful', () => {
    expect(
      loginReducer(undefined, {
        type: AUTHENTICATE_USER_ERROR
      })
    ).toEqual({
      status: AUTHENTICATE_USER_ERROR,
      error: true
    });
  });
});
