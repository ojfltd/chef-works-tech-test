import {
  authenticateUser,
  authenticateUserSuccess,
  authenticateUserError
} from '../actions';

import {
  AUTHENTICATE_USER_REQUESTED,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR
} from '../constants';

describe('Login actions', () => {
  describe('authenticate user action', () => {
    test('has a type of AUTHENTICATE_USER_REQUESTED', () => {
      const expected = {
        type: AUTHENTICATE_USER_REQUESTED,
        username: 'username',
        password: 'password'
      };
      expect(
        authenticateUser({ username: 'username', password: 'password' })
      ).toEqual(expected);
    });
  });

  describe('authenticate user success action', () => {
    test('has a type of AUTHENTICATE_USER_SUCCESS', () => {
      const expected = {
        type: AUTHENTICATE_USER_SUCCESS,
        error: false
      };
      expect(authenticateUserSuccess()).toEqual(expected);
    });
  });

  describe('authenticate user error action', () => {
    test('has a type of AUTHENTICATE_USER_ERROR', () => {
      const expected = {
        type: AUTHENTICATE_USER_ERROR,
        error: 'Bad things happened'
      };
      expect(authenticateUserError('Bad things happened')).toEqual(expected);
    });
  });
});
