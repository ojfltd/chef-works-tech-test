import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { browserHistory } from 'react-router-dom';

import { Login, mapDispatchToProps, mapStateToProps } from '../index';
import configureStore from '../../../configureStore';

import { changeLocale } from '../../LanguageProvider/actions';
import { authenticateUser } from '../actions';

describe('<Login />', () => {
  let store;

  beforeAll(() => {
    store = configureStore({}, browserHistory);
  });

  test('should render and match the snapshot', () => {
    const {
      container: { firstChild }
    } = render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <Login hasError={false} />
        </IntlProvider>
      </Provider>
    );
    expect(firstChild).toMatchSnapshot();
  });

  test('should toggle locale when language is selected', () => {
    const onLocaleToggleSpy = jest.fn();
    const { getByText } = render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <Login onLocaleToggle={onLocaleToggleSpy} />
        </IntlProvider>
      </Provider>
    );
    fireEvent.click(getByText('Français'));
    expect(onLocaleToggleSpy).toHaveBeenCalled();
  });

  test('should submit form when login button is clicked', () => {
    const onFormSubmitSpy = jest.fn();
    const { getByText } = render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <Login onFormSubmit={onFormSubmitSpy} />
        </IntlProvider>
      </Provider>
    );
    fireEvent.click(getByText('Login'));
    expect(onFormSubmitSpy).toHaveBeenCalled();
  });

  test('should update username in state', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <Login />
        </IntlProvider>
      </Provider>
    );
    fireEvent.change(getByTestId('username-input'), {
      target: { value: 'user.name' }
    });
  });

  test('should update username in state', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <Login />
        </IntlProvider>
      </Provider>
    );
    fireEvent.change(getByTestId('password-input'), {
      target: { value: 'password' }
    });
  });

  test('should render an error if login failed', async () => {
    const { findByTestId } = render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <Login hasError />
        </IntlProvider>
      </Provider>
    );
    const error = await findByTestId('login-error');
    expect(error).not.toBeNull();
  });

  describe('mapDispatchToProps', () => {
    describe('onLocaleToggle', () => {
      test('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onLocaleToggle).toBeDefined();
      });

      test('should dispatch onLocaleToggle when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onLocaleToggle({ target: { value: 'en' } });
        expect(dispatch).toHaveBeenCalledWith(changeLocale('en'));
      });
    });

    describe('onFormSubmit', () => {
      test('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onFormSubmit).toBeDefined();
      });

      test('should dispatch authenticateUser when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onFormSubmit({ username: 'user.name', password: 'password123' });
        expect(dispatch).toHaveBeenCalledWith(
          authenticateUser({ username: 'user.name', password: 'password123' })
        );
      });
    });
  });

  describe('mapStateToProps', () => {
    const result = mapStateToProps({
      language: { locale: 'en' },
      auth: { error: false }
    });

    test('should return the locale', () => {
      expect(result).toEqual({
        locale: 'en',
        hasError: false
      });
    });
  });
});
