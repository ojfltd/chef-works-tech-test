import {
  AUTHENTICATE_USER_REQUESTED,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR
} from './constants';

export const initialState = {
  status: undefined,
  error: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE_USER_REQUESTED:
      return {
        ...state,
        status: AUTHENTICATE_USER_REQUESTED
      };

    case AUTHENTICATE_USER_SUCCESS:
      return {
        ...state,
        status: AUTHENTICATE_USER_SUCCESS,
        token: action.token
      };

    case AUTHENTICATE_USER_ERROR:
      return {
        ...state,
        status: AUTHENTICATE_USER_ERROR,
        error: true
      };

    default:
      return state;
  }
};
