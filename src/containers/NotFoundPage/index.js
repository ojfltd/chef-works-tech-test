import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import Text from 'components/Text';
import messages from './messages';

export default function HomePage() {
  return (
    <>
      <Helmet>
        <title>Page not found</title>
      </Helmet>
      <div className="min-h-screen h-full py-10 bg-gray-100">
        <div className="container mx-auto">
          <Text>
            <FormattedMessage {...messages.header} />
          </Text>
        </div>
      </div>
    </>
  );
}
