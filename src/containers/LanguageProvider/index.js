import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { IntlProvider } from 'react-intl';

import { DEFAULT_LOCALE } from 'i18n';

export function LanguageProvider(props) {
  return (
    <IntlProvider
      key={props.locale}
      locale={props.locale}
      messages={props.messages[props.locale]}
    >
      {React.Children.only(props.children)}
    </IntlProvider>
  );
}

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired
};

LanguageProvider.defaultProps = {
  locale: DEFAULT_LOCALE
};

const mapStateToProps = ({ language: { locale } }) => ({
  locale
});

export default connect(mapStateToProps)(LanguageProvider);
