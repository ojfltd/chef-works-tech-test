import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import { FormattedMessage, defineMessages } from 'react-intl';

import ConnectedLanguageProvider, { LanguageProvider } from '../index';
import configureStore from '../../../configureStore';

import { translationMessages } from '../../../i18n';

const messages = defineMessages({
  someMessage: {
    id: 'some.id',
    defaultMessage: 'This is some default message',
    en: 'This is some en message'
  }
});

describe('<LanguageProvider />', () => {
  test('should render its children', () => {
    const children = <h1>Test</h1>;
    const { container } = render(
      <LanguageProvider locale="en" messages={messages}>
        {children}
      </LanguageProvider>
    );
    expect(container.firstChild).not.toBeNull();
  });
});

describe('<ConnectedLanguageProvider />', () => {
  let store;

  beforeAll(() => {
    store = configureStore({});
  });

  test('should render the default language messages', () => {
    const { queryByText } = render(
      <Provider store={store}>
        <ConnectedLanguageProvider messages={translationMessages}>
          <FormattedMessage {...messages.someMessage} />
        </ConnectedLanguageProvider>
      </Provider>
    );
    expect(queryByText(messages.someMessage.defaultMessage)).not.toBeNull();
  });
});
