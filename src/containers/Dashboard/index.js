import React from 'react';
import { Helmet } from 'react-helmet';

import ProductList from 'containers/ProductList';

export function Dashboard() {
  return (
    <>
      <Helmet>
        <title>Dashboard</title>
      </Helmet>
      <div className="min-h-screen h-full py-10 bg-gray-100">
        <div className="container mx-auto">
          <ProductList />
        </div>
      </div>
    </>
  );
}

export default Dashboard;
