import {
  GET_PRODUCT_LIST_REQUESTED,
  GET_PRODUCT_LIST_SUCCESS,
  GET_PRODUCT_LIST_ERROR
} from './constants';

export function getProductList() {
  return {
    type: GET_PRODUCT_LIST_REQUESTED
  };
}

export function getProductListSuccess(products) {
  return {
    type: GET_PRODUCT_LIST_SUCCESS,
    error: false,
    products
  };
}

export function getProductListError(error) {
  return {
    type: GET_PRODUCT_LIST_ERROR,
    error
  };
}
