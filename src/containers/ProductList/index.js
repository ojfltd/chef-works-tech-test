import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import LoadingIndicator from 'components/LoadingIndicator';
import ProductCard from 'components/ProductCard';
import Button from 'components/Button';
import Heading from 'components/Heading';
import Paragraph from 'components/Paragraph';
import Text from 'components/Text';
import ErrorSummary from 'components/ErrorSummary';

import { filterPlainArray } from 'utils';

import { getProductList } from './actions';

import { GET_PRODUCT_LIST_REQUESTED } from './constants';

export function ProductList({ getProductFeed, products, status, hasError }) {
  const [filters, setFilters] = useState({});
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    getProductFeed();
  }, []);

  useEffect(() => {
    setFilteredProducts(filterPlainArray(products, filters));
  }, [filters, products]);

  if (status === GET_PRODUCT_LIST_REQUESTED) {
    return <LoadingIndicator data-testid="product-list-error" />;
  }

  if (hasError) {
    return (
      <ErrorSummary
        data-testid="product-list-error"
        title="There was a problem retrieving the list of products"
      />
    );
  }

  const sizes = [...new Set(products.map(({ size }) => size))];
  const types = [...new Set(products.map(({ product_type: type }) => type))];
  const locations = [...new Set(products.map(({ location }) => location))];

  const toggleFilter = (key, value) => {
    const obj = { ...filters };

    if (obj[key] && obj[key].includes(value)) {
      obj[key] = [...obj[key], value];
    }

    if (obj[key]) {
      if (obj[key].includes(value)) {
        obj[key] = obj[key].filter(filterValue => filterValue !== value);
      } else {
        obj[key] = [...obj[key], value];
      }
    } else {
      obj[key] = [value];
    }

    setFilters(obj);
  };

  const resetFilter = () => setFilters({});

  return (
    <div className="p-5 grid grid-cols-1 md:grid-cols-3 gap-4">
      <aside className="col-span-1">
        <Paragraph data-testid="product-count">
          Showing <Text variation="bold">{filteredProducts.length}</Text>{' '}
          products
        </Paragraph>
        {[
          { label: 'Sizes', key: 'size', data: sizes },
          { label: 'Type', key: 'product_type', data: types },
          { label: 'Locations', key: 'location', data: locations }
        ].map(({ label, key, data }, i) => (
          <div key={i} className="mb-4">
            <Heading level={3}>{label}</Heading>
            <ul data-testid={`filter-${key}`}>
              {data.map((item, index) => (
                <li key={index} className="inline-block mr-2 mb-2">
                  <Button onClick={() => toggleFilter(key, item)}>
                    {item}{' '}
                    {filters[key] && filters[key].includes(item) ? (
                      <span className="rounded px-2 py-1 ml-1 text-sm bg-green-700">
                        ✕
                      </span>
                    ) : null}
                  </Button>
                </li>
              ))}
            </ul>
          </div>
        ))}
        {Object.keys(filters).length ? (
          <Button
            data-testid="reset-filters"
            variation="secondary"
            onClick={() => resetFilter()}
          >
            Reset filters
          </Button>
        ) : null}
      </aside>
      <div
        className="grid grid-cols-3 gap-4 col-span-2"
        data-testid="product-list"
      >
        {filteredProducts.length ? (
          filteredProducts.map((product, i) => (
            <ProductCard
              key={i}
              attributes={[
                { label: 'Type', value: product.product_type },
                { label: 'Size', value: product.size },
                { label: 'Stock', value: product.stock_level }
              ]}
              title={product.title}
            />
          ))
        ) : (
          <div className="col-span-3">
            <ErrorSummary title="No products match your criteria." />
          </div>
        )}
      </div>
    </div>
  );
}

ProductList.propTypes = {
  getProductFeed: PropTypes.func,
  products: PropTypes.array,
  status: PropTypes.string,
  hasError: PropTypes.bool
};

ProductList.defaultProps = {
  products: [],
  hasError: true
};

export const mapDispatchToProps = dispatch => ({
  getProductFeed: () => dispatch(getProductList()),
  dispatch
});

export const mapStateToProps = ({
  products: { products, status, error: hasError },
  language: { locale }
}) => ({
  products,
  locale,
  status,
  hasError
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
