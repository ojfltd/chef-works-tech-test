import { call, put, takeLatest } from 'redux-saga/effects';

import request from 'utils/request';

import { getProductListSuccess, getProductListError } from './actions';
import { GET_PRODUCT_LIST_REQUESTED } from './constants';

export function* getProductList() {
  try {
    const { products } = yield call(
      request,
      'https://xznrqupkl2.execute-api.eu-west-1.amazonaws.com/test/product-feed',
      {
        method: 'GET',
        headers: {
          Authorization: window.btoa(sessionStorage.getItem('token'))
        }
      }
    );
    yield put(getProductListSuccess(products));
  } catch (error) {
    yield put(getProductListError(error));
  }
}

export default function* productData() {
  yield takeLatest(GET_PRODUCT_LIST_REQUESTED, getProductList);
}
