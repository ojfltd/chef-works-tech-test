import {
  GET_PRODUCT_LIST_REQUESTED,
  GET_PRODUCT_LIST_SUCCESS,
  GET_PRODUCT_LIST_ERROR
} from './constants';

export const initialState = {
  products: [],
  error: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_LIST_REQUESTED:
      return {
        ...state,
        status: GET_PRODUCT_LIST_REQUESTED
      };

    case GET_PRODUCT_LIST_SUCCESS:
      return {
        ...state,
        status: GET_PRODUCT_LIST_SUCCESS,
        products: action.products
      };

    case GET_PRODUCT_LIST_ERROR:
      return {
        ...state,
        status: GET_PRODUCT_LIST_ERROR,
        error: true
      };

    default:
      return state;
  }
};
