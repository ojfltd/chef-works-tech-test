import React from 'react';
import { Provider } from 'react-redux';
import { fireEvent, render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { browserHistory } from 'react-router-dom';

import { ProductList, mapDispatchToProps, mapStateToProps } from '../index';
import configureStore from '../../../configureStore';

import { GET_PRODUCT_LIST_REQUESTED } from '../constants';

import { getProductList } from '../actions';

describe('<ProductList />', () => {
  let store;

  beforeAll(() => {
    store = configureStore({}, browserHistory);
  });

  const renderComponent = (props = {}) =>
    render(
      <Provider store={store}>
        <IntlProvider locale="en">
          <ProductList {...props} />
        </IntlProvider>
      </Provider>
    );

  test('should render and match the snapshot', () => {
    const {
      container: { firstChild }
    } = renderComponent({ getProductFeed: jest.fn, hasError: false });
    expect(firstChild).toMatchSnapshot();
  });

  test('should render an error if product list has error', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: true
    });
    const error = await findByTestId('product-list-error');
    expect(error).not.toBeNull();
  });

  test('should render a loading spinner when loading', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      status: GET_PRODUCT_LIST_REQUESTED
    });
    const error = await findByTestId('product-list-error');
    expect(error).not.toBeNull();
  });

  test('should render product cards', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: false,
      products: [
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'S',
          location: 'TORQUE',
          stock_level: 100
        }
      ]
    });
    const productList = await findByTestId('product-list');
    expect(productList.children).toHaveLength(1);
  });

  test('should render filters', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: false,
      products: [
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'S',
          location: 'TORQUE',
          stock_level: 100
        },
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'L',
          location: 'TORQUE',
          stock_level: 100
        }
      ]
    });

    const sizeFilter = await findByTestId('filter-size');
    expect(sizeFilter.children).toHaveLength(2);
  });

  test('will update the filters when clicked', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: false,
      products: [
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'S',
          location: 'TORQUE',
          stock_level: 100
        },
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'L',
          location: 'TORQUE',
          stock_level: 100
        }
      ]
    });

    const sizeFilter = await findByTestId('filter-size');
    fireEvent.click(sizeFilter.querySelector('button'));

    const productList = await findByTestId('product-list');
    expect(productList.children).toHaveLength(1);
  });

  test('will remove a filter when clicked a second time', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: false,
      products: [
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'S',
          location: 'TORQUE',
          stock_level: 100
        },
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'L',
          location: 'TORQUE',
          stock_level: 100
        }
      ]
    });

    const sizeFilter = await findByTestId('filter-size');
    fireEvent.click(sizeFilter.querySelector('button'));

    const productList = await findByTestId('product-list');
    expect(productList.children).toHaveLength(1);

    fireEvent.click(sizeFilter.querySelector('button'));
    expect(productList.children).toHaveLength(2);
  });

  test('will append a filter when one of the same type has already been selected', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: false,
      products: [
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'S',
          location: 'TORQUE',
          stock_level: 100
        },
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'L',
          location: 'TORQUE',
          stock_level: 100
        }
      ]
    });

    const sizeFilter = await findByTestId('filter-size');
    fireEvent.click(sizeFilter.querySelectorAll('button')[0]);

    const productList = await findByTestId('product-list');
    expect(productList.children).toHaveLength(1);

    fireEvent.click(sizeFilter.querySelectorAll('button')[1]);
    expect(productList.children).toHaveLength(2);
  });

  test('will render a reset button when filters are active', async () => {
    const { findByTestId } = renderComponent({
      getProductFeed: jest.fn,
      hasError: false,
      products: [
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'S',
          location: 'TORQUE',
          stock_level: 100
        },
        {
          id: 'RA2245-S',
          title: 'Red Apron 22',
          product_type: 'APRON',
          size: 'L',
          location: 'TORQUE',
          stock_level: 100
        }
      ]
    });
    const sizeFilter = await findByTestId('filter-size');
    fireEvent.click(sizeFilter.querySelector('button'));

    const resetButton = await findByTestId('reset-filters');
    expect(resetButton).not.toBeNull();

    fireEvent.click(resetButton);

    const productList = await findByTestId('product-list');
    expect(productList.children).toHaveLength(2);
  });

  describe('mapDispatchToProps', () => {
    describe('getProductFeed', () => {
      test('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.getProductFeed).toBeDefined();
      });

      test('should dispatch getProductFeed when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.getProductFeed({ target: { value: 'en' } });
        expect(dispatch).toHaveBeenCalledWith(getProductList());
      });
    });
  });

  describe('mapStateToProps', () => {
    const result = mapStateToProps({
      language: { locale: 'en' },
      products: { products: [{ id: 1 }], error: false }
    });

    test('should return the locale', () => {
      expect(result).toEqual({
        locale: 'en',
        hasError: false,
        products: [{ id: 1 }]
      });
    });
  });
});
