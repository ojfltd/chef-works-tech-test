import {
  getProductList,
  getProductListSuccess,
  getProductListError
} from '../actions';

import {
  GET_PRODUCT_LIST_REQUESTED,
  GET_PRODUCT_LIST_SUCCESS,
  GET_PRODUCT_LIST_ERROR
} from '../constants';

describe('ProductList actions', () => {
  describe('get product list action', () => {
    test('has a type of GET_PRODUCT_LIST_REQUESTED', () => {
      const expected = {
        type: GET_PRODUCT_LIST_REQUESTED
      };
      expect(getProductList()).toEqual(expected);
    });
  });

  describe('get product list success action', () => {
    test('has a type of GET_PRODUCT_LIST_SUCCESS', () => {
      const expected = {
        type: GET_PRODUCT_LIST_SUCCESS,
        error: false
      };
      expect(getProductListSuccess()).toEqual(expected);
    });
  });

  describe('get product list error action', () => {
    test('has a type of GET_PRODUCT_LIST_ERROR', () => {
      const expected = {
        type: GET_PRODUCT_LIST_ERROR,
        error: 'Bad things happened'
      };
      expect(getProductListError('Bad things happened')).toEqual(expected);
    });
  });
});
