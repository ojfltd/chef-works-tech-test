import { push } from 'connected-react-router';
import { put, takeLatest } from 'redux-saga/effects';

import { GET_PRODUCT_LIST_REQUESTED } from '../constants';
import { getProductListSuccess, getProductListError } from '../actions';
import productData, { getProductList } from '../saga';

const response = {
  products: [{ id: 1 }]
};

describe('product saga', () => {
  let generator;

  beforeEach(() => {
    generator = getProductList();
    generator.next();
  });

  test('should dispatch the getProductListSuccess action when successfull', () => {
    expect(generator.next(response).value).toEqual(
      put(getProductListSuccess(response.products))
    );
    // generator.next();
    // expect(generator.next().value).toEqual(put(push('/dashboard')));
  });

  test('should dispatch the getProductListError action when unsuccessful', () => {
    const error = new Error('Some error');
    expect(generator.throw(error).value).toEqual(
      put(getProductListError(error))
    );
  });
});

describe('productData Saga', () => {
  const userDataSaga = productData();

  test('should start task to watch for GET_PRODUCT_LIST_REQUESTED action', () => {
    const takeLatestDescriptor = userDataSaga.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(GET_PRODUCT_LIST_REQUESTED, getProductList)
    );
  });
});
