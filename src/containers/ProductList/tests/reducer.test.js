import productReducer from '../reducer';
import {
  GET_PRODUCT_LIST_REQUESTED,
  GET_PRODUCT_LIST_SUCCESS,
  GET_PRODUCT_LIST_ERROR
} from '../constants';

describe('productReducer', () => {
  test('returns the initial state', () => {
    expect(productReducer(undefined, {})).toEqual({
      error: false,
      status: undefined,
      products: []
    });
  });

  test('updated the status when product list is requested', () => {
    expect(
      productReducer(undefined, {
        type: GET_PRODUCT_LIST_REQUESTED
      })
    ).toEqual({
      status: GET_PRODUCT_LIST_REQUESTED,
      error: false,
      products: []
    });
  });

  test('updates the status when get product list is successful', () => {
    expect(
      productReducer(undefined, {
        type: GET_PRODUCT_LIST_SUCCESS,
        products: [{ id: 1 }]
      })
    ).toEqual({
      status: GET_PRODUCT_LIST_SUCCESS,
      error: false,
      products: [{ id: 1 }]
    });
  });

  test('updates the status and error when get product list is unsuccessful', () => {
    expect(
      productReducer(undefined, {
        type: GET_PRODUCT_LIST_ERROR
      })
    ).toEqual({
      status: GET_PRODUCT_LIST_ERROR,
      error: true,
      products: []
    });
  });
});
