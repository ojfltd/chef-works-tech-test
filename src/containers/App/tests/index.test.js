import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import ShallowRenderer from 'react-test-renderer/shallow';

import configureStore from '../../../configureStore';

import App, { Login, Dashboard, NotFoundPage } from '../index';

const renderer = new ShallowRenderer();

describe('<App />', () => {
  let store;

  beforeAll(() => {
    store = configureStore({});
  });

  test('should render and match the snapshot', () => {
    renderer.render(<App />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });

  [
    { name: 'Login', component: Login },
    { name: 'Dashboard', component: Dashboard },
    { name: 'NotFoundPage', component: NotFoundPage }
  ].forEach(({ name, component }) => {
    test(`<${name} /> is lazy loaded`, () => {
      const ComponentName = component;
      const { container } = render(
        <Provider store={store}>
          <Suspense fallback={<p>Loading</p>}>
            <ComponentName />
          </Suspense>
        </Provider>
      );

      expect(container.querySelector('p')).not.toBeNull();
    });
  });
});
