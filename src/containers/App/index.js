import React, { Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import LoadingIndicator from 'components/LoadingIndicator';

export const Login = React.lazy(() => import('containers/Login'));
export const Dashboard = React.lazy(() => import('containers/Dashboard'));
export const NotFoundPage = React.lazy(() => import('containers/NotFoundPage'));

export default function App() {
  return (
    <>
      <Helmet
        defaultTitle="React Application"
        titleTemplate="%s - React Application"
      ></Helmet>
      <Suspense fallback={<LoadingIndicator />}>
        <Switch>
          <Route exact component={Login} path="/" />
          <Route exact component={Dashboard} path="/dashboard" />
          <Route component={NotFoundPage} path="" />
        </Switch>
      </Suspense>
    </>
  );
}
