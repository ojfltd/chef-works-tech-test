import { slugify, formatDate, filterPlainArray, setAuthToken } from '../index';

describe('slugify', () => {
  test('removes spaces', () => {
    const expected = 'this-is-a-test';
    const actual = slugify('this is a test');
    expect(expected).toBe(actual);
  });

  test('converts to lowercase', () => {
    const expected = 'this-is-a-test';
    const actual = slugify('THIS IS A TEST');
    expect(expected).toBe(actual);
  });

  test('removes special characters', () => {
    const expected = 'thisis-atest';
    const actual = slugify('this+is-a&test');
    expect(expected).toBe(actual);
  });
});

describe('formatDate', () => {
  test('converts unix timestamp into human readable format', () => {
    const expected = '31 October 2019';
    const actual = formatDate('2019-10-31 12:30:00');
    expect(expected).toBe(actual);
  });
});

describe('filterPlainArray', () => {
  it('should filter an array of objects with one level-depth', () => {
    const products = [
      { name: 'A', color: 'Blue', size: 50 },
      { name: 'B', color: 'Blue', size: 60 },
      { name: 'C', color: 'Black', size: 70 },
      { name: 'D', color: 'Green', size: 50 }
    ];

    const filters = {
      color: ['BLUE', 'black'],
      size: [70, 50],
      ignored: []
    };

    const filtered = filterPlainArray(products, filters);
    const expected = [
      { name: 'A', color: 'Blue', size: 50 },
      { name: 'C', color: 'Black', size: 70 }
    ];
    expect(filtered).toEqual(expected);
  });
});

describe('setAuthToken', () => {
  it('should set a value in session storage', () => {
    setAuthToken('abc');
    expect(sessionStorage.setItem).toHaveBeenLastCalledWith('token', 'abc');
  });
});
