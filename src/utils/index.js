/**
 * Convert text into a kebab-case string suitable for URLs
 *
 * @param {string} text
 *
 * @return {string}
 */
export function slugify(text) {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '') // eslint-disable-line no-useless-escape
    .replace(/\-\-+/g, '-') // eslint-disable-line no-useless-escape
    .replace(/^-+/, '')
    .replace(/-+$/, '');
}

/**
 * Convert a timestamp into a date
 *
 * @param   {String|Integer}  timestamp  The timestamp in unix of YYYY-MM-DD HH:MM:SS format
 *
 * @returns {String}                     A formatted date
 */
export function formatDate(timestamp) {
  const date = new Date(timestamp);
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`;
}

/**
 * Filters an array of objects (one level-deep) with multiple criteria.
 *
 * @param  {Array}  array   the array to filter
 * @param  {Object} filters an object with the filter criteria
 * @return {Array}
 */
export function filterPlainArray(array, filters) {
  const getValue = value =>
    typeof value === 'string' ? value.toUpperCase() : value;

  const filterKeys = Object.keys(filters);
  return array.filter(item =>
    filterKeys.every(key => {
      // ignores an empty filter
      if (!filters[key].length) return true;
      return filters[key].find(
        filter => getValue(filter) === getValue(item[key])
      );
    })
  );
}

/**
 * Set the authentication token in session storage
 * @param {String} token the authentication token
 */
export function setAuthToken(token) {
  return sessionStorage.setItem('token', token);
}
