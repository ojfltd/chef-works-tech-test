export const locales = [
  {
    shortName: 'en',
    displayName: 'English'
  },
  {
    shortName: 'fr',
    displayName: 'Français'
  }
];
