import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'utils/history';

import authReducer from 'containers/Login/reducer';
import productsReducer from 'containers/ProductList/reducer';
import languageProviderReducer from 'containers/LanguageProvider/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    // global: globalReducer,
    router: connectRouter(history),
    language: languageProviderReducer,
    auth: authReducer,
    products: productsReducer,
    ...injectedReducers
  });

  return rootReducer;
}
