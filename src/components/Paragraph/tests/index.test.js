import React from 'react';
import { render } from '@testing-library/react';

import Paragraph from '../index';

const renderComponent = (props = {}) =>
  render(<Paragraph {...props}>This is some text</Paragraph>);

describe('<Paragraph />', () => {
  test('should render children', () => {
    const { container } = renderComponent();
    expect(container.querySelector('p').textContent).toBe('This is some text');
  });
});
