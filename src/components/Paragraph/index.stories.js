import React from 'react';
import { storiesOf } from '@storybook/react';

import Paragraph from '.';

const standard = () => (
  <>
    <Paragraph>Standard text</Paragraph>
    <Paragraph variation="large">Large text</Paragraph>
    <Paragraph variation="bold">Bold text</Paragraph>
  </>
);

storiesOf('Paragraph', module).add('standard', standard);
