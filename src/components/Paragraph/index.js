import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Paragraph({ children, variation, ...attrs }) {
  const classes = classNames('mb-2 text-gray-700', {
    'text-base': variation !== 'large',
    'text-lg': variation === 'large',
    'font-bold': variation === 'bold'
  });

  return (
    <p className={classes} {...attrs}>
      {children}
    </p>
  );
}

Paragraph.propTypes = {
  variation: PropTypes.oneOf(['large', 'bold', 'normal']),
  children: PropTypes.node.isRequired
};

Paragraph.defaultTypes = {
  variation: 'normal'
};

export default Paragraph;
