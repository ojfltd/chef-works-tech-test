import React from 'react';
import { render } from '@testing-library/react';

import Heading from '../index';

const renderComponent = (props = {}) =>
  render(<Heading {...props}>This is a heading</Heading>);

describe('<Heading />', () => {
  test('should render children', () => {
    const { container } = renderComponent({ level: 1 });
    expect(container.querySelector('h1').textContent).toBe('This is a heading');
  });
});
