import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Heading({ level, children, ...attr }) {
  const classes = classNames('text-gray-700', {
    'text-3xl': level === 1,
    'text-2xl': level === 2,
    'text-xl': level === 3,
    'text-lg': level === 4,
    'text-base': level === 5,
    'text-sm': level === 6
  });

  return React.createElement(`h${level}`, { className: classes, ...attr }, [
    ...children
  ]);
}

Heading.propTypes = {
  level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
  children: PropTypes.node.isRequired
};

Heading.defaultTypes = {
  level: 1
};

export default Heading;
