import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Input({
  type,
  name,
  disabled,
  error,
  value,
  placeholder,
  autoComplete,
  required,
  onChange,
  onBlur,
  id,
  'data-testid': testId,
  ...attrs
}) {
  const classes = classNames(
    'p-2 w-full border border-gray-400 rounded-sm text-gray-700 focus:outline-none focus:border-gray-500',
    {
      'opacity-50': disabled,
      'border-red-400': error
    }
  );

  return (
    <input
      autoComplete={autoComplete}
      className={classes}
      data-testid={testId}
      defaultValue={value}
      disabled={disabled}
      id={id}
      name={name}
      placeholder={placeholder}
      required={required}
      type={type}
      onBlur={onBlur}
      onChange={onChange}
      {...attrs}
    />
  );
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'email', 'password', 'search', 'tel']),
  disabled: PropTypes.bool,
  value: PropTypes.string,
  error: PropTypes.bool,
  placeholder: PropTypes.string,
  autoComplete: PropTypes.string,
  required: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  'data-testid': PropTypes.string,
  id: PropTypes.string.isRequired
};

Input.defaultProps = {
  disabled: false,
  type: 'text',
  value: undefined
};

export default Input;
