import React from 'react';
import { storiesOf } from '@storybook/react';

import Input from '.';

const primary = () => <Input />;
const error = () => <Input error />;
const disabled = () => <Input disabled value="This is disabled" />;
const extraAttributes = () => <Input inputmode="numeric" pattern="[0-9]*" />;

storiesOf('Input', module)
  .add('primary', primary)
  .add('error', error)
  .add('disabled', disabled)
  .add('extra attributes', extraAttributes);
