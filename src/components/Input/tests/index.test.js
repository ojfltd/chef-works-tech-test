import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import Input from '../index';

const renderComponent = (props = {}) => render(<Input {...props} />);

describe('<Input />', () => {
  ['text', 'email', 'password', 'search', 'tel'].forEach(type => {
    test(`should adopt a ${type} type attribute`, () => {
      const { container } = renderComponent({
        type,
        id: 'input-1',
        name: 'input-1'
      });
      expect(container.querySelector('input').getAttribute('type')).toBe(type);
    });
  });

  test('should adopt a text type attribute when one is not supplied', () => {
    const { container } = renderComponent({ id: 'input-1', name: 'input-1' });
    expect(container.querySelector('input').getAttribute('type')).toBe('text');
  });

  test('should adopt any additional attributes supplied', () => {
    const { container } = renderComponent({
      pattern: '[0-9]*',
      inputMode: 'numeric',
      id: 'input-1',
      name: 'input-1'
    });
    expect(container.querySelector('input').getAttribute('pattern')).toBe(
      '[0-9]*'
    );
    expect(container.querySelector('input').getAttribute('inputmode')).toBe(
      'numeric'
    );
  });

  test('should handle change events', () => {
    const onChangeSpy = jest.fn();
    const { container } = renderComponent({
      onChange: onChangeSpy,
      id: 'input-1',
      name: 'input-1'
    });
    fireEvent.change(container.querySelector('input'), {
      target: { value: 'text input' }
    });
    expect(onChangeSpy).toHaveBeenCalled();
  });

  test('should handle blur events', () => {
    const onBlurSpy = jest.fn();
    const { container } = renderComponent({
      onBlur: onBlurSpy,
      id: 'input-1',
      name: 'input-1'
    });
    fireEvent.blur(container.querySelector('input'));
    expect(onBlurSpy).toHaveBeenCalled();
  });
});
