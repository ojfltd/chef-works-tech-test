import React from 'react';
import { storiesOf } from '@storybook/react';

import Field from '.';

const standard = () => (
  <Field
    required
    autoComplete="current-password"
    data-testid="password-input"
    id="password"
    label="Password"
    name="password"
    type="password"
  />
);

const withError = () => (
  <Field
    required
    autoComplete="current-password"
    data-testid="password-input"
    error="Please enter your password"
    id="password"
    label="Password"
    name="password"
    type="password"
  />
);

const withHint = () => (
  <Field
    required
    autoComplete="current-password"
    data-testid="password-input"
    hint="Please enter your password"
    id="password"
    label="Password"
    name="password"
    type="password"
  />
);

const textarea = () => (
  <Field
    required
    data-testid="description-textarea"
    hint="Please enter a description"
    id="description"
    label="Description"
    name="description"
    type="textarea"
  />
);

storiesOf('Field', module)
  .add('standard', standard)
  .add('with error', withError)
  .add('with hint', withHint)
  .add('textarea', textarea);
