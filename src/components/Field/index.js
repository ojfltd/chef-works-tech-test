import React from 'react';
import PropTypes from 'prop-types';

import Input from 'components/Input';
import Textarea from 'components/Textarea';
import Label from 'components/Label';
import ErrorMessage from 'components/ErrorMessage';
import HintText from 'components/HintText';

function Field({
  'data-testid': testId,
  autoComplete,
  disabled,
  error,
  hint,
  id,
  label,
  name,
  onBlur,
  onChange,
  placeholder,
  required,
  type,
  value,
  ...attrs
}) {
  return (
    <div className="mb-4">
      <Label htmlFor={id}>{label}</Label>
      {hint ? <HintText id={`${id}-hint`}>{hint}</HintText> : null}
      <ErrorMessage text={error} />
      {type === 'textarea' ? (
        <Textarea
          aria-describedby={hint ? `${id}-hint` : null}
          data-testid={testId}
          disabled={disabled}
          error={Boolean(error)}
          id={id}
          name={name}
          placeholder={placeholder}
          required={required}
          value={value}
          onBlur={onBlur}
          onChange={onChange}
          {...attrs}
        />
      ) : (
        <Input
          aria-describedby={hint ? `${id}-hint` : null}
          autoComplete={autoComplete}
          data-testid={testId}
          disabled={disabled}
          error={Boolean(error)}
          id={id}
          name={name}
          placeholder={placeholder}
          required={required}
          type={type}
          value={value}
          onBlur={onBlur}
          onChange={onChange}
          {...attrs}
        />
      )}
    </div>
  );
}

Field.propTypes = {
  'data-testid': PropTypes.string,
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  hint: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.oneOf([
    'text',
    'email',
    'password',
    'search',
    'tel',
    'textarea'
  ]),
  value: PropTypes.string
};

export default Field;
