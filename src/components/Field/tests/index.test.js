import React from 'react';
import { render } from '@testing-library/react';

import Field from '../index';

const renderComponent = (props = {}) => render(<Field {...props} />);

describe('<Field />', () => {
  test('should render a label linked to an input', () => {
    const { container } = renderComponent({ id: 'abc' });
    expect(container.querySelector('label').getAttribute('for')).toBe('abc');
    expect(container.querySelector('input').getAttribute('id')).toBe('abc');
  });

  ['text', 'email', 'password', 'search', 'tel'].forEach(type => {
    test(`should render an input for ${type} type`, () => {
      const { container } = renderComponent({ type, id: 'abc' });
      expect(container.querySelectorAll('input')).toHaveLength(1);
    });
  });

  ['textarea'].forEach(type => {
    test(`should render an input for ${type} type`, () => {
      const { container } = renderComponent({ type, id: 'abc' });
      expect(container.querySelectorAll('textarea')).toHaveLength(1);
    });
  });

  test('should render a hint', () => {
    const { container } = renderComponent({
      id: 'password',
      hint: 'Your password should be 6 characters or more'
    });
    expect(container.querySelector('#password-hint').textContent).toBe(
      'Your password should be 6 characters or more'
    );
  });

  test('should render a hint for a textarea', () => {
    const { container } = renderComponent({
      id: 'description',
      hint: 'Your description should be 6 characters or more',
      type: 'textarea'
    });
    expect(container.querySelector('#description-hint').textContent).toBe(
      'Your description should be 6 characters or more'
    );
  });
});
