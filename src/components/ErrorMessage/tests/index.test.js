import React from 'react';
import { render } from '@testing-library/react';

import ErrorMessage from '../index';

const renderComponent = (props = {}) => render(<ErrorMessage {...props} />);

describe('<ErrorMessage />', () => {
  test('should render text', () => {
    const { container } = renderComponent({ text: 'Something went wrong' });
    expect(container.querySelector('span').textContent).toBe(
      'Error: Something went wrong'
    );
  });
});
