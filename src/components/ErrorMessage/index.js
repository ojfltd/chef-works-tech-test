import React from 'react';
import PropTypes from 'prop-types';

function ErrorMessage({ text }) {
  return (
    <span
      aria-live="assertive"
      aria-relevant="additions removals"
      className="mb-1 block text-sm text-red-500"
    >
      <span className="sr-only">Error:</span> {text}
    </span>
  );
}

ErrorMessage.propTypes = {
  text: PropTypes.oneOf([PropTypes.string, PropTypes.node])
};

export default ErrorMessage;
