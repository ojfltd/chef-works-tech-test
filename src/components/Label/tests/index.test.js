import React from 'react';
import { render } from '@testing-library/react';

import Label from '../index';

const label = 'A label';
const renderComponent = (props = {}) =>
  render(<Label {...props}>{label}</Label>);

describe('<Label />', () => {
  test('should have children', () => {
    const { container } = renderComponent({ htmlFor: 'username' });
    expect(container.querySelector('label').innerHTML).toBe(label);
  });
});
