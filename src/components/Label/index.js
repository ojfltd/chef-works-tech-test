import React from 'react';
import PropTypes from 'prop-types';

function Label({ htmlFor, children }) {
  return (
    <label className="mb-1 block text-gray-700" htmlFor={htmlFor}>
      {children}
    </label>
  );
}

Label.propTypes = {
  htmlFor: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default Label;
