import React, { Children } from 'react';
import { Link as ReactRouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';

function Link({ href, external, children, className }) {
  const attrs = {
    target: external && '_blank',
    rel: external && 'noopener noreferrer',
    className
  };

  if (!external) {
    return (
      <ReactRouterLink className={className} to={href}>
        {Children.toArray(children)}
      </ReactRouterLink>
    );
  }

  return (
    <a href={href} {...attrs}>
      {Children.toArray(children)}
    </a>
  );
}

Link.propTypes = {
  href: PropTypes.string.isRequired,
  external: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

Link.defaultProps = {
  external: false
};

export default Link;
