import React from 'react';
import { render } from '@testing-library/react';
import { Router } from 'react-router-dom';

import history from 'utils/history';

import Link from '../index';

const href = 'https://google.com';
const children = <p>Test</p>;
const renderComponent = (props = {}) =>
  render(
    <Router history={history}>
      <Link {...props}>{children}</Link>
    </Router>
  );

describe('<Link />', () => {
  test('should render an <a> tag if a href is specified', () => {
    const { container } = renderComponent({ href });
    expect(container.querySelector('a')).not.toBeNull();
  });

  test('should have children', () => {
    const { container } = renderComponent({ href });
    expect(container.querySelector('a').children).toHaveLength(1);
  });

  test('should adopt a target attribute', () => {
    const { container } = renderComponent({ href, external: true });
    expect(container.querySelector('a').getAttribute('target')).toBe('_blank');
  });

  test('should adopt a rel attribute', () => {
    const { container } = renderComponent({ href, external: true });
    expect(container.querySelector('a').getAttribute('rel')).toBe(
      'noopener noreferrer'
    );
  });

  test('should not adopt a target attribute when external', () => {
    const { container } = renderComponent({ href, external: false });
    expect(container.querySelector('a').getAttribute('target')).toBe(null);
  });

  test('should not adopt a rel attribute when external', () => {
    const { container } = renderComponent({ href, external: false });
    expect(container.querySelector('a').getAttribute('rel')).toBe(null);
  });
});
