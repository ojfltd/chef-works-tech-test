import styled from 'styled-components';

const StyledSpinnerContainer = styled.div`
  display: inline-block;
  position: relative;

  width: 64px;
  height: 64px;
`;

export default StyledSpinnerContainer;
