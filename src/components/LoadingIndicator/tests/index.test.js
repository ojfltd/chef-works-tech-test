import React from 'react';
import { render } from '@testing-library/react';

import LoadingIndicator from '../index';

const renderComponent = () => render(<LoadingIndicator />);

describe('<LoadingIndicator />', () => {
  test('should render a <div> tag', () => {
    const { container } = renderComponent();
    expect(container.querySelector('div')).not.toBeNull();
  });
});
