import React from 'react';
import PropTypes from 'prop-types';

import StyledSpinnerContainer from './StyledSpinnerContainer';
import StyledSpinnerQuadrant from './StyledSpinnerQuadrant';

function LoadingIndicator({ 'data-testid': testId }) {
  return (
    <StyledSpinnerContainer data-testid={testId}>
      <StyledSpinnerQuadrant />
      <StyledSpinnerQuadrant />
      <StyledSpinnerQuadrant />
      <StyledSpinnerQuadrant />
    </StyledSpinnerContainer>
  );
}

LoadingIndicator.propTypes = {
  'data-testid': PropTypes.string
};

export default LoadingIndicator;
