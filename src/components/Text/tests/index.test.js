import React from 'react';
import { render } from '@testing-library/react';

import Text from '../index';

const renderComponent = (props = {}) =>
  render(<Text {...props}>This is some text</Text>);

describe('<Text />', () => {
  test('should render children', () => {
    const { container } = renderComponent();
    expect(container.querySelector('span').textContent).toBe(
      'This is some text'
    );
  });
});
