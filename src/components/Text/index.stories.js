import React from 'react';
import { storiesOf } from '@storybook/react';

import Text from '.';

const standard = () => (
  <>
    <Text>Standard text</Text>
    <Text variation="large">Large text</Text>
    <Text variation="bold">Bold text</Text>
  </>
);

storiesOf('Text', module).add('standard', standard);
