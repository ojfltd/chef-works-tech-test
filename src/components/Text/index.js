import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Text({ children, variation, ...attr }) {
  const classes = classNames('text-gray-700', {
    'text-base': variation !== 'large',
    'text-lg': variation === 'large',
    'font-bold': variation === 'bold'
  });

  return (
    <span className={classes} {...attr}>
      {children}
    </span>
  );
}

Text.propTypes = {
  variation: PropTypes.oneOf(['large', 'bold', 'normal']),
  children: PropTypes.node.isRequired
};

Text.defaultTypes = {
  variation: 'normal'
};

export default Text;
