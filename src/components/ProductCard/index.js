import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Heading from 'components/Heading';
import Text from 'components/Text';

function ProductCard({ title, attributes }) {
  const wrapperClasses = classNames('p-3 bg-white rounded shadow-sm');

  return (
    <article className={wrapperClasses}>
      <Heading level={3}>{title}</Heading>
      {attributes.length ? (
        <ul>
          {attributes.map(({ label, value }) => (
            <li>
              <Text variation="bold">{label}:</Text> <Text>{value}</Text>
            </li>
          ))}
        </ul>
      ) : null}
    </article>
  );
}

ProductCard.propTypes = {
  attributes: PropTypes.array,
  title: PropTypes.string.isRequired
};

ProductCard.defaultProps = {
  attributes: []
};

export default ProductCard;
