import React from 'react';
import { render } from '@testing-library/react';

import ErrorSummary from '../index';

const renderComponent = (props = {}) => render(<ErrorSummary {...props} />);

describe('<ErrorSummary />', () => {
  test('should render a title', () => {
    const { container } = renderComponent({ title: 'Something went wrong' });
    expect(container.querySelector('div').textContent).toBe(
      'Something went wrong'
    );
  });

  test('should render a list of errors when supplied', () => {
    const { container } = renderComponent({
      title: 'Something went wrong',
      items: [
        { text: 'Issue 1', href: '#input-1' },
        { text: 'Issue 2', href: '#input-2' }
      ]
    });
    expect(container.querySelectorAll('li').length).toBe(2);
  });
});
