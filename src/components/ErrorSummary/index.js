import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

function ErrorSummary({ title, items, 'data-testid': testId }) {
  const error = useRef();

  // ensure the error gets focus when it appears on screen
  useEffect(() => error.current && error.current.focus());

  return (
    <div
      ref={error}
      aria-labelledby="error-summary-title"
      className="mb-3 p-2 bg-red-200 border border-red-600 rounded-sm text-red-700"
      data-testid={testId}
      role="alert"
      tabIndex="-1"
    >
      <h2 id="error-summary-title">{title}</h2>
      {items.length ? (
        <ul className="mt-2 ml-5">
          {items.map(({ text, href }, i) => (
            <li key={i} className="list-disc">
              <a className="underline hover:no-underline" href={href}>
                {text}
              </a>
            </li>
          ))}
        </ul>
      ) : null}
    </div>
  );
}

ErrorSummary.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      href: PropTypes.string
    })
  ),
  'data-testid': PropTypes.string
};

ErrorSummary.defaultProps = {
  items: []
};

export default ErrorSummary;
