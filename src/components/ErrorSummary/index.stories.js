import React from 'react';
import { storiesOf } from '@storybook/react';

import ErrorSummary from '.';

const standard = () => <ErrorSummary title="There were some errors" />;
const withList = () => (
  <ErrorSummary
    items={[
      { text: 'Issue 1', href: '#input-1' },
      { text: 'Issue 2', href: '#input-2' }
    ]}
    title="There is a problem"
  />
);

storiesOf('ErrorSummary', module)
  .add('standard', standard)
  .add('with list', withList);
