import React from 'react';
import { render } from '@testing-library/react';

import HintText from '../index';

const renderComponent = (props = {}) =>
  render(<HintText {...props}>This is a hint</HintText>);

describe('<HintText />', () => {
  test('should render children', () => {
    const { container } = renderComponent({ id: 'fancy-hint' });
    expect(container.querySelector('span').textContent).toBe('This is a hint');
  });

  test('should adopt an id', () => {
    const { container } = renderComponent({ id: 'fancy-hint' });
    expect(container.querySelector('span').getAttribute('id')).toBe(
      'fancy-hint'
    );
  });
});
