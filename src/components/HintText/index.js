import React from 'react';
import PropTypes from 'prop-types';

function HintText({ id, children }) {
  return (
    <span className="mb-1 block text-gray-500" id={id}>
      {children}
    </span>
  );
}

HintText.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default HintText;
