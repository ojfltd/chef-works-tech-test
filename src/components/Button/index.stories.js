import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from '.';

const primary = () => (
  <Button onClick={action('clicked')}>Primary Button</Button>
);
const secondary = () => (
  <Button variation="secondary" onClick={action('clicked')}>
    Secondary Button
  </Button>
);

storiesOf('Button', module)
  .add('primary', primary)
  .add('secondary', secondary);
