import React, { Children } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Button({
  type,
  href,
  disabled,
  full,
  onClick,
  children,
  variation,
  'data-testid': testId
}) {
  if (href) {
    return (
      <a data-testid={testId} disabled={disabled} href={href}>
        {Children.toArray(children)}
      </a>
    );
  }

  const classes = classNames(
    'px-3 py-2 rounded-sm text-white focus:outline-none',
    {
      'opacity-50 cursor-not-allowed': disabled,
      'bg-green-500 hover:bg-green-600 focus:bg-green-600':
        variation === 'primary',
      'text-green-500 border border-green-500 hover:text-green-600 hover:border-green-600 focus:text-green-600 focus:border-green-600':
        variation === 'secondary',
      'w-full': full === true
    }
  );

  return (
    // eslint-disable-next-line react/button-has-type
    <button
      className={classes}
      data-testid={testId}
      disabled={disabled}
      type={type}
      onClick={onClick}
    >
      {Children.toArray(children)}
    </button>
  );
}

Button.propTypes = {
  'data-testid': PropTypes.string,
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
  variation: PropTypes.oneOf(['primary', 'secondary']),
  disabled: PropTypes.bool,
  href: PropTypes.string,
  full: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired
};

Button.defaultProps = {
  disabled: false,
  onClick: undefined,
  type: 'button',
  variation: 'primary',
  full: false
};

export default Button;
