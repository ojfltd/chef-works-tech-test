import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import Button from '../index';

const href = 'https://google.com';
const children = <p>Test</p>;
const renderComponent = (props = {}) =>
  render(<Button {...props}>{children}</Button>);

describe('<Button />', () => {
  test('should render an <a> tag if a href is specified', () => {
    const { container } = renderComponent({ href });
    expect(container.querySelector('a')).not.toBeNull();
  });

  test('should have children', () => {
    const { container } = renderComponent({ href });
    expect(container.querySelector('a').children).toHaveLength(1);
  });

  test('should handle click events', () => {
    const onClickSpy = jest.fn();
    const { container } = renderComponent({ onClick: onClickSpy });
    fireEvent.click(container.querySelector('button'));
    expect(onClickSpy).toHaveBeenCalled();
  });

  test('should not adopt a type attribute when rendering an <a> tag', () => {
    const type = 'text/html';
    const { container } = renderComponent({ href, type });
    expect(container.querySelector(`a[type="${type}"]`)).toBeNull();
  });

  test('should default to a type attribute of button when rendering a button', () => {
    const { container } = renderComponent();
    expect(container.querySelector('button').getAttribute('type')).toBe(
      'button'
    );
  });

  test('should adopt the supplied type attribute when rendering a button', () => {
    const type = 'submit';
    const { container } = renderComponent({ type });
    expect(container.querySelector('button').getAttribute('type')).toBe(
      'submit'
    );
  });
});
