import React from 'react';
import { storiesOf } from '@storybook/react';

import Textarea from '.';

const primary = () => <Textarea />;
const error = () => <Textarea error />;
const disabled = () => <Textarea disabled value="This is disabled" />;

storiesOf('Textarea', module)
  .add('primary', primary)
  .add('error', error)
  .add('disabled', disabled);
