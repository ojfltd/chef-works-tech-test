import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import Textarea from '../index';

const renderComponent = (props = {}) => render(<Textarea {...props} />);

describe('<Textarea />', () => {
  test('should handle change events', () => {
    const onChangeSpy = jest.fn();
    const { container } = renderComponent({
      onChange: onChangeSpy,
      id: 'textarea-1',
      name: 'textarea-1'
    });
    fireEvent.change(container.querySelector('textarea'), {
      target: { value: 'textarea input' }
    });
    expect(onChangeSpy).toHaveBeenCalled();
  });

  test('should handle blur events', () => {
    const onBlurSpy = jest.fn();
    const { container } = renderComponent({
      onBlur: onBlurSpy,
      id: 'textarea-1',
      name: 'textarea-1'
    });
    fireEvent.blur(container.querySelector('textarea'));
    expect(onBlurSpy).toHaveBeenCalled();
  });
});
