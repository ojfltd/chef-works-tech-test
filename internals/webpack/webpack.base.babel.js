const path = require('path');
const webpack = require('webpack');

module.exports = options => ({
  mode: options.mode,

  entry: options.entry,

  output: {
    ...options.output,
    path: path.resolve(process.cwd(), 'dist'),
    publicPath: '/'
  },

  optimization: options.optimization,

  plugins: options.plugins.concat([
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
  ]),

  devtool: options.devtool,

  performance: options.performance || {},

  devServer: options.devServer,

  module: {
    rules: options.module.rules.concat([
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: ['file-loader']
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          'postcss-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/env',
                {
                  targets: ['> 1%']
                }
              ],
              '@babel/react'
            ],
            plugins: [
              '@babel/proposal-class-properties',
              '@babel/syntax-dynamic-import'
            ]
          }
        }
      },
      {
        test: /\.html$/,
        use: ['html-loader']
      }
    ])
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json', '.css'],
    modules: [
      path.resolve(process.cwd(), 'node_modules'),
      path.resolve(process.cwd(), 'src')
    ]
  }
});
