import { configure, addDecorator } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';

import '../src/style.css';

const req = require.context('../src', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => {
    console.log(filename);
    return req(filename);
  });
}

addDecorator(withA11y);

configure(loadStories, module);
