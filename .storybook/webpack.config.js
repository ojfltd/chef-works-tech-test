const path = require('path');

module.exports = async ({ config, mode }) => {
  config.module.rules.push({
    test: /\.css$/,
    use: [
      {
        loader: 'postcss-loader',
        options: {
          ident: 'postcss',
          config: {
            path: './'
          }
        }
      }
    ],
    include: path.resolve(__dirname, '../')
  });

  config.resolve = Object.assign(config.resolve, {
    extensions: ['.js', '.jsx', '.json', '.css'],
    modules: [
      path.resolve(process.cwd(), 'node_modules'),
      path.resolve(process.cwd(), 'src')
    ]
  });

  return config;
};
