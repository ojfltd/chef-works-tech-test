pipeline {
    agent any

    options { disableConcurrentBuilds() }

    stages {
        stage('Prepare') {
            steps {
                sh 'npm ci'
            }
        }

        stage('Formating & Linting') {
            steps {
                sh 'npm run lint'
            }
        }

        stage('Unit Tests') {
            steps {
                sh 'npm run test:coverage'
            }
            post {
                always {
                    cobertura coberturaReportFile: 'coverage/cobertura-coverage.xml'
                }
            }
        }

        stage('Build') {
            steps {
                sh 'npm run build'
            }
        }

        stage('Automation Tests') {
            steps {
                sh 'npm run cypress:ci'
            }
        }

        stage('Deploy') {
            steps {
                echo 'Deploying...'
            }

            post {
                success {
                    slackSend channel: "#builds", color: "good", message: "${JOB_NAME} - ${BUILD_DISPLAY_NAME} - Success (<${BUILD_URL}|Open>)"
                }

                failure {
                    slackSend channel: "#builds", color: "danger", message: "${JOB_NAME} - ${BUILD_DISPLAY_NAME} - Failure (<${BUILD_URL}|Open>)"
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
